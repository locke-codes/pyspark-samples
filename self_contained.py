from pyspark.sql import SparkSession

spark = SparkSession \
        .builder \
        .appName("simpleApp") \
        .getOrCreate()

logFile = "sample_text.txt"  # Should be some file on your system
logData = spark.read.text(logFile).cache()

numAs = logData.filter(logData.value.contains("a")).count()
numBs = logData.filter(logData.value.contains("b")).count()

print("Lines with a: %i, lines with b: %i" % (numAs, numBs))

spark.stop()
