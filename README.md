spin up the data

    docker-compose up --build -d

access the data

    PGPASSWORD=test psql -h localhost -p 5432 -U test -d source_db

process data

modify `sample_process_data.py`

    docker-compose build && docker-compose run spark python3 /data/sample_process_data.py

run any spark python file you drop in here with

    docker-compose build && docker-compose run spark python3 /data/{filename}