"""
NOTE: This data processing isn't valid. Just tinkering
"""
import os
from pyspark.sql import SparkSession
from _db_init import db_properties, db_url

spark = SparkSession \
        .builder \
        .appName("LoadTestFile") \
        .config("spark.jars", os.getenv("SPARK_CLASSPATH")) \
        .getOrCreate()

print("read database table into frame")
dad = spark.read.jdbc(url=db_url, table="drug_alcohol_deaths", properties=db_properties)
print("show data frame from database")
dad.show()

print("read database table into frame")
ad = spark.read.jdbc(url=db_url, table="accidental_deaths", properties=db_properties)
print("show data frame from database")
ad.show()

print("join some data")
cf16 = ad.join(
    dad,
    dad.County == ad.ResidenceCounty,
    "inner"
) \
.select(
    dad.County.alias("county"),
    (ad.Fentanyl == 'Y').alias("is_fentanyl"),
    (dad["2016"]).alias("year")
).toDF(
    "county", "is_fentanyl", "total_2016_drug_deaths"
)
cf16.createOrReplaceTempView("cf16")

spark.sql(
    """
    select
        county,
        total_2016_drug_deaths,
        sum(
            case
                when is_fentanyl then 1
                else 0
            end
        ) as fent_deaths
    from cf16
    group by county, total_2016_drug_deaths
    """
).show()

spark.stop()
