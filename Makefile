build:
	docker-compose build

up: build
	docker-compose up -d

FILE=''
run: build
	docker-compose run spark python3 /data/${FILE}

FILE=''
run-company: build
	docker-compose -f docker-compose.yaml -f docker-compose.company.yaml run spark python3 /data/${FILE}
